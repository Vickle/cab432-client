//import logo from './logo.svg';
import './App.css';
import SearchBar from "./searchBars"; 

function App() {
  return (
    <div className="App">
     <h1>Pokemon Gym Simulator</h1>
     <SearchBar />
    </div>
  );
}

export default App;
