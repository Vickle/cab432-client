import React, { useEffect, useState } from "react";
import axios from "axios";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import "./App.css"
import "./table.css";
const SearchBar = () => {
    // These are the states that will contain the pokemon names, the winner of the battle and each pokemon's statistics
    const [pokeone, setPokeone] = useState("");
    const [poketwo, setPoketwo] = useState("");
    const [pokemonOne, setPokemonOne] = useState("");
    const [pokemonTwo, setPokemonTwo] = useState("");
    const [winner, setWinner] = useState("");
    const [pokeoneattack, setPokeOneattack] = useState("");
    const [pokeonedefence, setPokeOnedefence] = useState("");
    const [pokeonespecialatt, setPokeOnespecialatt] = useState("");
    const [pokeonespecialdef, setPokeOnespecialdef] = useState("");
    const [pokeonespeed, setPokeOnespeed] = useState("");
    const [pokeonehp, setPokeOnehp] = useState("");
    const [poketwoattack, setPokeTwoattack] = useState("");
    const [poketwodefence, setPokeTwodefence] = useState("");
    const [poketwospecialatt, setPokeTwospecialatt] = useState("");
    const [poketwospecialdef, setPokeTwospecialdef] = useState("");
    const [poketwospeed, setPokeTwospeed] = useState("");
    const [poketwohp, setPokeTwohp] = useState("");
    // This use effect hook will get the data from the server and set the states
    // The useEffect hook only takes place when pokemonTwo is set 
    useEffect(() => {
        if (pokemonOne === "" || pokemonTwo === "") {
            window.alert("Please enter two Pokemon that is in the Pokedex database")
        }
        else {
            axios.get(`api/search?pokeone=${pokemonOne}&poketwo=${pokemonTwo}&pokeonefound=false&poketwofound=false`).then((res) => {
                
                if(res.data.ERROR)
                {

                    window.alert("Pokemon not found")

                }
                else{

                setPokeOnehp(res.data.pokeonearray[0].base_stat);
                setPokeOneattack(res.data.pokeonearray[1].base_stat);
                setPokeOnedefence(res.data.pokeonearray[2].base_stat);
                setPokeOnespecialatt(res.data.pokeonearray[3].base_stat);
                setPokeOnespecialdef(res.data.pokeonearray[4].base_stat);
                setPokeOnespeed(res.data.pokeonearray[5].base_stat);
                setPokeTwohp(res.data.poketwoarray[0].base_stat);
                setPokeTwoattack(res.data.poketwoarray[1].base_stat);
                setPokeTwodefence(res.data.poketwoarray[2].base_stat);
                setPokeTwospecialatt(res.data.poketwoarray[3].base_stat);
                setPokeTwospecialdef(res.data.poketwoarray[4].base_stat);
                setPokeTwospeed(res.data.poketwoarray[5].base_stat);
                if (res.data === "noData") {
                    window.alert("Please enter two Pokemon that is in the Pokedex database")
                }
                setWinner(res.data.winner);
            }
            });
        };

    }, [pokemonTwo]);

    // columns and rows are the used to define the data in the table 
    var columns = [
        {
            headerName: "Statistic",
            field: "stat",
        },
        {
            headerName: pokemonOne,
            field: "firstPoke",
        },
        {
            headerName: pokemonTwo,
            field: "secondPoke",
        }
    ];

    var rows = [
        { stat: 'hp', firstPoke: pokeonehp, secondPoke: poketwohp },
        { stat: 'attack', firstPoke: pokeoneattack, secondPoke: poketwoattack },
        { stat: 'defence', firstPoke: pokeonedefence, secondPoke: poketwodefence },
        { stat: 'special-attack', firstPoke: pokeonespecialatt, secondPoke: poketwospecialatt },
        { stat: 'special-defence', firstPoke: pokeonespecialdef, secondPoke: poketwospecialdef },
        { stat: 'speed', firstPoke: pokeonespeed, secondPoke: poketwospeed },
    ];
    // These functions take place after the button is clicked. It sets the state of pokemonOne and pokemonTwo
    const handleSubmit = (e) => {
        e.preventDefault();
        setPokemonOne(pokeone);
        setPokemonTwo(poketwo);
        
    }


    return (

        <div>

            <form name="myform" onSubmit={handleSubmit} >
                <input type="text" name="pokemon_one" required placeholder="First Pokemon" value={pokeone} onChange={(e) => setPokeone(e.target.value)} />
                <input type="text" name="pokemon_two" required placeholder="Second Pokemon" value={poketwo} onChange={(e) => setPoketwo(e.target.value)} />

                <button type="submit">Battle</button>
            </form>
            {
                <p>Winner: {winner}</p>
            }
            <div className="ag-theme-balham"
                style={{
                    marginLeft: "auto",
                    marginRight: "auto",
                    height: "250px",
                    width: "600px",
                }}>
                <AgGridReact
                    columnDefs={columns}
                    rowData={rows}
                    ></AgGridReact>
            </div>

        </div>

    );
}

export default SearchBar;